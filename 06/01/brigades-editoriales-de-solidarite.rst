


.. _bes_2024_06_01:

====================================================
2024-06-01 Soutien à l'Ukraine résistante N°30
====================================================

:download:`Soutien à l'Ukraine résistante 1er juin 2024 N°30 <pdfs/30_soutien-a-l-ukraine-resistante_2024_06_01.pdf>`
