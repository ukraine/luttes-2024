

.. _resu_2024_06_07:

===================================================================================================================================================================
2024-06-07 À nos soeurs et frères ukrainien·nes, d’Ukraine, des territoires occupés et de la diaspora – CP du RESU France du 07 juin 2024 |solidarite_ukraine|
===================================================================================================================================================================

- https://aplutsoc.org/2024/06/08/a-nos-soeurs-et-freres-ukrainien%c2%b7nes-dukraine-des-territoires-occupes-et-de-la-diaspora-cp-du-resu-france-du-07-juin-2024/
- https://aplutsoc.org/wp-content/uploads/2024/06/resu-france_2024-06-07_a_nos-soeurs-et-freres-ua.pdf
- https://entreleslignesentrelesmots.wordpress.com/2024/06/08/tribune-de-collectifs-de-solidarite-a-loccasion-de-la-visite-de-zelensky-a-madrid/


:download:`Télécharger le fichier PDF <pfds/resu-france_2024-06-07_a_nos-soeurs-et-freres-ua.pdf>`


À nos soeurs et frères ukrainien·nes, d’Ukraine, des territoires occupés et de la diaspora
===============================================================================================

À nos sœurs et frères ukrainien·nes, d’Ukraine, des territoires occupés et
de la diaspora Nous nous adressons à vous en ce lendemain des commémorations
du débarquement de Normandie et en cette veille des élections au Parlement
européen pour saluer la présence de votre président, Volodymyr Zelensky,
à l’Assemblée nationale.

Il incarne votre résistance à l’impérialisme russe et c’est pour cette raison
que nous pensons que la gauche de France, à laquelle nous appartenons, attachée
à l’émancipation humaine, aurait dû saluer à cette occasion.

Malheureusement, nous avons le devoir de vous dire qu’aujourd’hui nous avons
eu honte du visage que certain·es parlementaires de gauche ont montré à
l’Assemblée nationale.

**Honte** de l’absence de la majorité d’entre elles et d’entre eux des bancs de l’Assemblée.

**Honte** de l’appel lancé par un député, LFI Jérôme Legavre, à priver l’Ukraine
d’armes et à un cessez-le-feu immédiat.

Une telle posture assure la poursuite de la russification, des crimes de guerre,
des crimes contre l’humanité dans les zones occupées.

**Honte** de ses mensonges sur l’Ukraine qui s’inscrivent dans la tradition policière
et tortionnaire du stalinisme, des chars à Budapest en 1956 et à Prague en 1968
et des tirs sur les ouvriers de Gdansk en 1981.

**Honte** des déclarations du dirigeant du PC Fabien Roussel accusant Volodymyr
Zelensky de nous entraîner dans la troisième guerre mondiale.

**Honte** des mensonges laissant croire que la France entrerait prétendument en
guerre en fournissant à l’Ukraine les armes dont elle a tant besoin, alors
qu’en réalité Emmanuel Macron, dans son intervention du jeudi 6 juin 2024,
a laissé ouverte la possibilité d’abandonner le Donbass ou une partie du
Donbass à Poutine.

Cher·es sœurs et frères ukrainien·nes, d’Ukraine, des territoires occupés
et de la diaspora, nous sommes de cette gauche – syndicale, associative,
politique – qui vous soutient et qui sait que le seul avenir pour elle passe
par la défense de votre combat national et démocratique armé.

**Nous sommes la gauche** qui se bat inlassablement pour que revive l’internationalisme
et qui repousse le mensonge triomphant qui passe.

**Nous sommes la gauche** qui se tient à vos côtés, pour le droit des peuples à
l’autodétermination, pour le respect du droit international, pour le droit
à l’autodéfense, pour les droits et libertés démocratiques des femmes et des
hommes d’Ukraine, dans les entreprises, les bureaux, les universités et sous
les drapeaux.

7 juin 2024

ukrainesolidaritefrance@gmail.com

Comité Français du Réseau Européen de Solidarité avec l’Ukraine

